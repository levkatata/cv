import styles from './skills.module.css';

export function Skills({id}) {
    return (
        <div className={styles.divmain} id={id}>
            <label>Skills</label>
            <table>
                <tbody>
                    <tr>
                        <td>Web Frontend Development (HTML/CSS/JavaScript/React/Redux)</td>
                        <td>2022 - current time</td>
                    </tr>
                    <tr>
                        <td>GitLab</td>
                        <td>2022 - current time</td>
                    </tr>
                    <tr>
                        <td>Mobile Development (Android/Java)</td>
                        <td>2009 - 2013</td>
                    </tr>
                    <tr>
                        <td>Desktop Development (C++/MFC/wxWidgets)</td>
                        <td>2005 - 2007</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}