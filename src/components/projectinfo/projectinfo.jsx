import styles from './projectinfo.module.css';
import Carousel from 'react-gallery-carousel';
import 'react-gallery-carousel/dist/index.css';
import { useContext } from 'react';
import { ScreenContext } from '../app/App';

export function ProjectInfo(props) {
    const isWideScreen = useContext(ScreenContext);

    return (<div className={styles.divmain}>
        <div className={styles.divtitle}>{props.title}</div>
        <div className={`${styles.divdescription} ${!isWideScreen && styles.divdescription_vertical}`}>
            {props.children}
            <Carousel images={[
                {src: props.img1, style: {objectFit: 'contain', objectPosition: 'bottom', marginTop: "auto", marginBottom: "auto"}}, 
                {src: props.img2, style: {objectFit: 'contain', objectPosition: 'bottom', marginTop: "auto", marginBottom: "auto"}}, 
                {src: props.img3, style: {objectFit: 'contain', objectPosition: 'bottom', marginTop: "auto", marginBottom: "auto"}}]}
                style={{width: isWideScreen ? "40%" : "80%",
                    backgroundColor: 'transparent',
                    objectPosition: 'center center'}}
                    hasMediaButton={false}
                    hasSizeButton={false}
                    hasThumbnails={false}
                    hasCaptions={false}
                    autoPlayInterval={5000}
                    isAutoPlaying={true}
                    hasIndexBoard={false}/>
        </div>
    </div>);
}