import styles from './hello.module.css';
import Photo from '../../images/my_photo.jpg';
import { useContext } from 'react';
import { ScreenContext } from '../app/App';

export function Hello() {
    const isWideScreen = useContext(ScreenContext);
    console.dir(isWideScreen);

    return (
        <div className={`${styles.divmain} ${!isWideScreen ? styles.divmain_vertical : ""}`}>
            <section className={`${styles.sectiontext} ${!isWideScreen ? styles.sectiontext_vertical : ""}`}>
                <label>Mobile Android/Front-end Developer</label>
                <label>Hello, my name is Tetiana Triapichko!</label>
                {
                    !isWideScreen ? 
                        (<section className={styles.sectionphoto}>
                            <img src={Photo} alt="my photo"/>
                        </section>) 
                        : ""
                }
                <label>I'm software developer with 5+ years of expirience. Will glad to provide the project of Your dream to the life!</label>
            </section>
            {
                isWideScreen ? 
                    (<section className={styles.sectionphoto}>
                        <img src={Photo} alt="my photo"/>
                    </section>)
                    : ""
            }
            
        </div>
    );
}