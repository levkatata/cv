import styles from './about.module.css';

export function About({id}) {
    return (
        <div className={styles.divmain} id={id}>
            <label>About</label>
            <section>My working way started in 2006. I was a 4th year student and worked as a Desktop
                Software Developer in C++ for OS Windows. Than I had learned Android development and
                tried myself in that way. I was enjoying this work, but I must give it up because of
                my maternity leave. For some family reasons I couldn't work for a long time,
                but I was learning new software development technologies and doing some my projects.
                Now I'd really like to return to Software Development and work on interesting 
                project.</section>
        </div>
    );
}