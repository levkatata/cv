import styles from './contacts.module.css';
import EmailImg from '../../images/email.png';
import PhoneImg from '../../images/phone.png';
import TelegramImg from '../../images/telegram.png';
import ViberImg from '../../images/viber.png';

export function Contacts({id}) {
    return (
        <div className={styles.divmain} id={id}>
            <label>My Contacts</label>
            <table>
                <tbody>
                    <tr>
                        <td><a href='mailto:levkatata@gmail.com'><img src={EmailImg} alt="email"/></a></td>
                        <td><a href='mailto:levkatata@gmail.com'>levkatata@gmail.com</a></td>
                    </tr>
                    <tr>
                        <td><a href='tel:+380953388556'><img src={PhoneImg} alt="email"/></a>
                            <a href='viber://contact?number=%2B380953388556'><img src={ViberImg} alt="viber"/></a>
                            <a href='https://t.me/+380953388556'><img src={TelegramImg} alt="email"/></a></td>
                        <td><a href='https://t.me/+380953388556'>+380953388556</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}