import './header.module.css';

export function Header(props) {
    return (
        <header>
            <label>Tetiana Triapichko</label>
            <ul>
                <li onClick={props.onClickProjects}>Projects</li>
                <li onClick={props.onClickAbout}>About</li>
                <li onClick={props.onClickSkills}>Skills</li>
                <li onClick={props.onClickContacts}>Contacts</li>
            </ul>
        </header>
    );
}