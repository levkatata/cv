import { createContext, useState, useEffect } from 'react';
import { About } from '../about/about';
import { Contacts } from '../contacts/contacts';
import { Header } from '../header/header';
import { Hello } from '../hello/hello';
import { Projects } from '../projects/projects';
import { Skills } from '../skills/skills';
import './App.css';

export const ScreenContext = createContext();

function App() {
  const options = ["projects", "skills", "about", "contacts"];
  const mediaQuery = '(min-width: 768px)';
  const [isWideScreen, setWideScreen] = useState(window.matchMedia(mediaQuery).matches);
  
  useEffect(() => {
      const listener = (e) => {
          setWideScreen(e.matches);
          // console.log("Media event listener" + e.matches);
      }

      window.matchMedia(mediaQuery).addEventListener("change", listener);
      return () => {
          window.matchMedia(mediaQuery).removeEventListener("change", listener);
      }
  }, []); 

  const showProjects = () => {
    window.location.href = "#" + options[0];
  };
  const showSkills = () => {
    window.location.href = "#" + options[1];
  };
  const showAbout = () => {
    window.location.href = "#" + options[2];
  };
  const showContacts = () => {
    window.location.href = "#" + options[3];
  }

  return (
    <ScreenContext.Provider value={isWideScreen}>
      <div className="App">
        <Header 
          onClickProjects={showProjects}
          onClickSkills={showSkills}
          onClickAbout={showAbout}
          onClickContacts={showContacts}/>
        <Hello/>
        <Projects id={options[0]}/>
        <Skills id={options[1]}/>
        <About id={options[2]}/>
        <Contacts id={options[3]}/>
      </div>
    </ScreenContext.Provider>
  );
}

export default App;
