import { ProjectInfo } from '../projectinfo/projectinfo';
import styles from './project.module.css';
import CakeImg1 from '../../images/cake_project_1.png';
import CakeImg2 from '../../images/cake_project_2.png';
import CakeImg3 from '../../images/cake_project_3.png';
import ShopImg1 from '../../images/shop_project_1.png';
import ShopImg2 from '../../images/shop_project_2.png';
import ShopImg3 from '../../images/shop_project_3.png';

export function Projects(props) {

    return (
        <div className={styles.divmain} id={props.id}>
            <label>Projects</label>
            <ProjectInfo img1={CakeImg1} img2={CakeImg2} img3={CakeImg3}
                title="Miss Cupcake">
                <section>This is a web-site for viewing handmade pancakes, reading description, making 
                    order and pay for it. The web-site is fully adaptived for different devices and 
                    has a lot of different animations. In this project I am responsible for creating 
                    UI using HTML/CSS, edting basket using JavaScript and adding animation using WOWJS.
                    You are welcome to view my&nbsp;
                    <a href='https://gitlab.com/levkatata/jseducation/-/tree/master/HomeWork20-Cakes?ref_type=heads'>code in git</a>.
                </section>
            </ProjectInfo>
            <ProjectInfo img1={ShopImg1} img2={ShopImg2} img3={ShopImg3}
                title="SportIF E-Shop">
                <section>SportIF is a e-shop with sport and usual clothes. Here you can view collections
                    by categories, add items to the basket and edit it. Also there is an ability to apply
                    a filter clothes by color and by size. Collections are displaying by using paginations. 
                    Shop data are getting from server using FETCH request. Pages are created using 
                    HTML/CSS/JavaScript/BootStrap by myself. E-shop is compleitly adaptived. Please, view&nbsp;
                    <a href='https://gitlab.com/levkatata/jseducation/-/tree/master/HomeWork15-Shop?ref_type=heads'>my code is here</a>.
                </section>
            </ProjectInfo>
        </div>
    );
}